const express = require('express');
const router = express.Router();

const diagnosisController = require('../controllers/diagnosis');

// CRUD
router.post('/', diagnosisController.create);

router.get('/', diagnosisController.getAll);

router.get('/:id', diagnosisController.get);

// router.patch('/:id', diagnosisController.update);

router.delete('/:id', diagnosisController.delete);


module.exports = router;