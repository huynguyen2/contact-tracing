FROM node:14-alpine as development

WORKDIR /app

COPY package.json .

RUN npm install

RUN npm install -g nodemon

COPY . .

EXPOSE 8080

CMD ["npm", "run", "dev"]

FROM node:14-alpine as production

WORKDIR /app

COPY package.json .

RUN npm install

COPY . .

EXPOSE 80

CMD ["npm", "start"]