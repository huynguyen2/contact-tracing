const fs = require("fs");
const ByteBuffer = require("bytebuffer");

const writeBufferFile = (key, rollingTime, date) => {
  // const path = "/var/www/downloads/2020-12-10/";
  const path = `/var/www/downloads/${date}/`;
  // const file = "a.dat";
  const file = `dk_data_${date}_0.dat`;
  // const key = "010203040506070809";

  var x = rollingTime;

  try {
    if (fs.existsSync(path)) {
      //file exists

      console.log("exist");
    } else {
      fs.mkdirSync(path);

      console.log("non exist");
    }

    var bb = ByteBuffer.fromHex(key);

    console.log(new Buffer(bb.toArrayBuffer()));

    var bytes = new Array(4);

    bytes[0] = x & 255;

    x = x >> 8;

    bytes[1] = x & 255;

    x = x >> 8;

    bytes[2] = x & 255;

    x = x >> 8;

    bytes[3] = x & 255;

    console.log(bytes);

    fs.appendFile(path + file, new Buffer(bb.toArrayBuffer()), function (err) {
      if (err) throw err;

      fs.appendFile(path + file, Buffer.from(bytes), function (err) {
        if (err) throw err;
      });
    });

    //fs.writeFile(path + file, Buffer.from(bytes), function(err) {

    // if (err) throw err;

    // });
  } catch (err) {
    console.error(err);
  }
};

module.exports = writeBufferFile;
