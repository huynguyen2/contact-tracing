const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const diagnosisRouter = require('./routes/diagnosis');

const PORT = process.env.PORT;
const app = express();

app.use(cors());

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/diagnosis', diagnosisRouter);

app.listen(PORT, console.log(`App running, listen port ${PORT}`));

module.exports = app;