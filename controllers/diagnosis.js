const fs = require('fs');
const { Op } = require("sequelize");
const neatCSV = require('neat-csv');
const json2csv = require('json2csv');
const Diagnosis = require('../models/diagnosis');
const writeBufferFile = require('../utils/writeBufferFile');

exports.create = async (req, res) => {
  const { diagnosis_key, rolling_time, date } = req.body;
  const fields = ['date', 'total_keys', 'url'];
  const pathFileName = '/var/www/downloads/dk_index.csv';

  try {
    const diagnosis = await Diagnosis.create({
      diagnosis_key,
      rolling_time,
      date
    });

    writeBufferFile(diagnosis_key, rolling_time, date);

    // dataCSV
    let dataCSV = [];
    // check exist
    if (fs.existsSync(pathFileName)) {
      const readFileStream = fs.createReadStream(pathFileName);
      dataCSV = await neatCSV(readFileStream);
      readFileStream.close();
    }
    
    const removeFormatDate = date.split('-').join('');
    const linkDownload = `http://185.202.20.227/downloads/${date}/dk_data_${date}_0.dat`

    const record = dataCSV.find(data => data.date === removeFormatDate && data.url === linkDownload);
    console.log(record);

    if (record) {
      record.total_keys = (parseInt(record.total_keys) + 1).toString();
    } else {
      dataCSV.push({
        date: removeFormatDate,
        total_keys: '1',
        url: linkDownload
      });
    }

    // dataCSV.sort((a, b) => a.date - b.date);
    const csv = await json2csv.parse(dataCSV, { fields, quote: '' }) + '\r\n';
    fs.writeFile(pathFileName, csv, (err) => {
      if (err) {
        throw err;
      }
      fs.chmodSync(pathFileName, 0o755);
      res.status(201).json(diagnosis);
    });
    
  } catch (error) {
    res.status(400).json(error);
  }
}

exports.get = async (req, res) => {
  const diagnosis_key = req.params.id;

  try {
    const diagnosis = await Diagnosis.findOne({
      where: {
        diagnosis_key
      } 
    });
  
    res.json(diagnosis);
  } catch (error) {
    res.status(400).json(error);
  }
}

exports.getAll = async (req, res) => {
  try {
    const allDiagnosis = await Diagnosis.findAll();
    res.json(allDiagnosis);
  } catch (error) {
    res.status(400).json(error);
  }
}

exports.update = async (req, res) => {
  const diagnosis_key = req.params.id;
  const { rolling_time, date } = req.body;

  try {
    await Diagnosis.update({ rolling_time, date }, {
      where: {
        diagnosis_key
      }
    });

    res.json({ message: 'Update successful' });
  } catch (error) {
    res.status(400).json(error);
  }
}

exports.delete = async (req, res) => {
  const diagnosis_key = req.params.id;
  const fields = ['date', 'total_keys', 'url'];
  const pathFileName = '/var/www/downloads/dk_index.csv';

  try {
    const diagnosis = await Diagnosis.findOne({
      where: {
        diagnosis_key
      } 
    });

    if (diagnosis) {
      const path = `/var/www/downloads/${diagnosis.dataValues.date}/`;
      const filePath = path + `dk_data_${diagnosis.dataValues.date}_0.dat`;
      fs.stat(filePath, (err, stats) => {
        if (err) {
          return console.error(err);
        }
        
        console.log(stats);

        fs.unlink(filePath, async (err) => {
          if (err) {
            return console.error(err);
          }

          await Diagnosis.destroy({
            where: {
              diagnosis_key: diagnosis.dataValues.diagnosis_key
            } 
          });

          // Write file again exclude deleted record
          const diagnosises = await Diagnosis.findAndCountAll({
            where: {
              date: {
                [Op.eq]: diagnosis.dataValues.date
              }
            }
          });

          if (diagnosises.count > 0) {
            diagnosises.rows.forEach(({ dataValues: { diagnosis_key, rolling_time, date } }) => {
              writeBufferFile(diagnosis_key, rolling_time, date);
            });
          }

          // dataCSV
          let dataCSV = [];
          // check exist
          if (fs.existsSync(pathFileName)) {
            const readFileStream = fs.createReadStream(pathFileName);
            dataCSV = await neatCSV(readFileStream);
            readFileStream.close();
          }

          const removeFormatDate = diagnosis.dataValues.date.split('-').join('');
          const linkDownload = `http://185.202.20.227/downloads/${diagnosis.dataValues.date}/dk_data_${diagnosis.dataValues.date}_0.dat`

          const recordIndex = dataCSV.findIndex(data => data.date === removeFormatDate && data.url === linkDownload);
          console.log(dataCSV[recordIndex]);

          if (recordIndex !== -1) {
            dataCSV[recordIndex].total_keys = (parseInt(dataCSV[recordIndex].total_keys) - 1);
            if (dataCSV[recordIndex].total_keys <= 0) {
              dataCSV.splice(recordIndex, 1);
            }
          }

          const csv = await json2csv.parse(dataCSV, { fields, quote: '' }) + '\r\n';
          fs.writeFile(pathFileName, csv, (err) => {
            if (err) {
              throw err;
            }
            fs.chmodSync(pathFileName, 0o755); 
            res.json({ message: 'Delete successful'});
          });
        });
      });
    } else {
      res.status(400).json({ error: 'Diagnosis key invalid'});
    }

  } catch (error) {
    res.status(400).json(error);
  }
}