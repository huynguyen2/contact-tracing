const { DataTypes } = require("sequelize");
const sequelize = require("../utils/db");

const diagnosisModel = sequelize.define(
  "diagnosis",
  {
    diagnosis_key: {
      // autoIncrement: true,
      type: DataTypes.STRING(45),
      allowNull: false,
      primaryKey: true,
    },
    rolling_time: {
      type: DataTypes.INTEGER,
    },
    date: {
      type: DataTypes.DATEONLY,
    },
  },
  {
    sequelize,
    tableName: "diagnosis",
    timestamps: false,
  }
);

module.exports = diagnosisModel;
